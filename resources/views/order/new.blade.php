@extends('layouts.app')

@section('title')
    Nuevo pedido
@endsection

@section('body')

<section class="vbox">
    <section class="scrollable padder">
        <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
            <li><a href="index.html"><i class="fa fa-home"></i> Inicio</a></li>
            <li class="active">#</li>
        </ul>
        <div class="m-b-md">
            <h3 class="m-b-none">Nuevo pedido</h3>
        </div>
        <section class="panel panel-default">
            <header class="panel-heading">
                Formulario nuevo pedido
                <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i>
            </header>

            <div class="panel-body">
                {!! Form::open(array('route'=>'order.store')) !!}
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Nombre</label>
                            {!! Form::text('name', '', ['placeholder'=>'Ingrese su nombre completo', 'class'=>'form-control input-lg','required']) !!}
                        </div>
                        <div class="form-group">
                            <label>CI /NIT</label>
                            {!! Form::text('phone', '', ['placeholder'=>'Ingrese su CI /NIT', 'class'=>'form-control input-lg','required']) !!}
                        </div>
                        <div class="form-group">
                            <label>Dirección</label>
                            {!! Form::textarea('address', '', ['placeholder'=>'Ingrese su dirección', 'class'=>'form-control input-lg','rows'=>'3','required']) !!}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Fecha</label>
                            {!! Form::date('delivery_date', \Carbon\Carbon::now(), [ 'class'=>'form-control input-lg','required']) !!}
                        </div>
                        <div class="form-group">
                            <label>Menu</label>
                            {!! Form::select('product_id', $product, null, ['id'=>'product_id', 'class'=>'form-control m-b input-lg','required']) !!}
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Tipo</label>
                                    {!! Form::select('payment_option', array( 'Mesanine' => 'Mesanine', 'Para llevar' => 'Para llevar'), null, ['class'=>'form-control m-b input-lg','required']) !!}
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Cantidad</label>
                                    {!! Form::number('quantity', '1', ['placeholder'=>'Ingrese cantidad', 'class'=>'form-control input-lg', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="line line-dashed line-lg pull-in"></div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2">Estado</label>
                                <div class="col-sm-10 ">
                                    <label class="radio-custom col-md-2 input-md">{!! Form::radio('order_status', 'Confirm') !!}<i class="fa fa-circle-o fa-1x"></i>Confirmar </label>
                                    <label class="radio-custom col-md-2 input-md">{!! Form::radio('order_status', 'Ready') !!}<i class="fa fa-circle-o"></i>Menu listo </label>
                                    <label class="radio-custom col-md-2 input-md">{!! Form::radio('order_status', 'Send') !!}<i class="fa fa-circle-o"></i>En espera </label>
                                    <label class="radio-custom col-md-2 input-md">{!! Form::radio('order_status', 'Delivered') !!}<i class="fa fa-circle-o"></i>Entregado </label>
                                    <label class="radio-custom col-md-2 input-md">{!! Form::radio('order_status', 'Returned') !!}<i class="fa fa-circle-o"></i>Retornado </label>
                                    <label class="radio-custom col-md-2 input-md">{!! Form::radio('order_status', 'Cancelled') !!}<i class="fa fa-circle-o"></i>Cancelado </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="line line-dashed line-lg pull-in"></div>
                        {!! Form::submit('Enviar', [ 'class'=>'btn btn-default']) !!}
                    </div>
                {!! Form::close() !!}
            </div>

        </section>
    </section>
</section>

@endsection