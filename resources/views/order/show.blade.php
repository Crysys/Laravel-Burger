@extends('layouts.app')

@section('title')
	Imprimir pedido
@endsection

@section('body')

<section class="vbox bg-white">
    <header class="header b-b b-light hidden-print">
        <button href="#" class="btn btn-sm btn-info pull-right" onClick="window.print();">Imprimir</button>
        <p>Recibo</p>
    </header>
    <section class="scrollable wrapper" id="print">
        <div class="row">
            <div class="col-xs-6">
                <h2 style="margin-top: 0px">MESTIZO <b>Servicio Web</b></h2>
                <p>Avenida Simón I. Patiño <br>
                    0000, Vinto<br>
                    Cochabamba, Bolivia
                </p>
            </div>
            <div class="col-xs-6 text-right">
                <h4>Recibo</h4>
            </div>
        </div>
        <div class="well m-t" style="margin-bottom: 50px">
            <div class="row">
                <div class="col-xs-6">
                    <strong>Para:</strong>
                    <h4>{{ $order->name }}</h4>
                    <p>
                        {{ $order->address }}
                    </p>
                    <b>CI / NIT: </b>{{ $order->phone }}
                </div>
                <div class="col-xs-6 text-right">
                    <p class="h4">#{{ $order->id }}</p>
                    <h5>Fecha: <strong>{{ $order->delivery_date }}</strong></h5>
                    <p class="m-t m-b">Pedido n°: <strong>{{ $order->id }}</strong></p>
                </div>
            </div>
        </div>
        <div class="line"></div>
        <table class="table">
            <thead>
            <tr>
                <th width="60">Cantidad</th>
                <th>Descripción</th>
                <th width="120">Precio</th>
                <th width="120">Total</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{ $order->quantity }}</td>
                <td><b>{{ $order->product->id }}. {{ $order->product->product_name }}</b><br><small>{{ $order->product->product_details }}</small></td>
                <td class="">Bs. {{ $order->product->price }}</td>
                <td class="">Bs. {{ $order->product->price * $order->quantity }}</td>
            </tr>
            <tr>
                <td colspan="3" class="text-right"><strong>Subtotal</strong></td>
                <td>Bs. {{ $order->product->price * $order->quantity }}</td>
            </tr>
            <tr>
                <td colspan="3" class="text-right no-border"><strong>IVA</strong></td>
                <td>Bs. 0.00</td>
            </tr>
            <tr>
                <td colspan="3" class="text-right no-border"><strong>Total</strong></td>
                <td><strong>Bs. {{ $order->product->price * $order->quantity }}</strong></td>
            </tr>
            </tbody>
        </table>
        <div class="row">
            <div class="col-xs-8">
                <p style="text-align: justify;"><i> Mestizo, servicio de comidas</i></p><br><br>

                <p>Recibido: __________________ </p>
            </div>
        </div>
    </section>
</section>

@endsection